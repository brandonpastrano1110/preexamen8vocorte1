function hacerPeticion() {
    const url = "https://jsonplaceholder.typicode.com/users";
    // Utilizando Axios para realizar la solicitud GET
    axios.get(url)
        .then(response => {
            // Aquí se maneja la respuesta
            let res = document.getElementById("idV");
            const datoss = response.data;
            let idE = 0;
            // Ciclo para tomar cada uno de los registros
            for (const datos of datoss) {
                if (res.value == datos.id.toString()) {
                    let name = document.getElementById("name");
                    name.value = datos.name;
                    let username = document.getElementById("username");
                    username.value = datos.username;
                    let email = document.getElementById("email");
                    email.value = datos.email;
                    let calle = document.getElementById("calle");
                    calle.value = datos.address.street;
                    let numero = document.getElementById("numero");
                    numero.value = datos.address.suite;
                    let ciudad = document.getElementById("ciudad");
                    ciudad.value = datos.address.city;
                    idE = 1;
                    break;
                }
            }
            if (idE != 1) {
                idE = 0;
                alert("No se encontro el id");
            }
        })
        .catch(error => {
            console.error('Error al realizar la solicitud:', error);
        });
}
function limpiar() {
    let id = document.getElementById("idV");
    id.value="";
    let name = document.getElementById("name");
    name.value = "";
    let username = document.getElementById("username");
    username.value = "";
    let email = document.getElementById("email");
    email.value = "";
    let calle = document.getElementById("calle");
    calle.value = "";
    let numero = document.getElementById("numero");
    numero.value = "";
    let ciudad = document.getElementById("ciudad");
    ciudad.value = "";
}  
document.getElementById("btnBuscar").addEventListener("click", function () {
    hacerPeticion();
});

document.getElementById("btnLimpiar").addEventListener("click", function () {
    limpiar();
});