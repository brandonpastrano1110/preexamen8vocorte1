//manejando promesas 
llamandoFetch = () => {
    const res = document.getElementById('pais').value;
    const url = "https://restcountries.com/v3.1/name/" + res
    fetch(url)
        .then(respuesta => respuesta.json())
        .then(data => buscarPais(data))
        .catch((reject) => {
            alert("No se encontró el país.");
            console.log("Surgió el siguiente error: " + reject);
        });
}
function buscarPais(data) { 
    for (let item of data) {
        let cap = document.getElementById("capital");
        if (cap) cap.value = item.capital;

        let lenguaje = document.getElementById("lenguaje");
        if (lenguaje) {
            if (typeof item.languages === "object") {
                const languageValues = Object.values(item.languages);
                if (languageValues.length > 0) {
                    lenguaje.value = languageValues[0];  
                } else {
                    lenguaje.value = "";  
                }
            } else {
                lenguaje.value = item.languages;  
            }
        }
 
    }
}
document.getElementById("btnBuscar").addEventListener('click', function () {
    llamandoFetch();
});
document.getElementById("btnLimpiar").addEventListener('click', function () {
    const res = document.getElementById('pais');
    res.value = "";
    let cap = document.getElementById("capital");
    cap.value = "";
    let lenguaje = document.getElementById("lenguaje");
    lenguaje.value = "";
});